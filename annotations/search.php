<?php
if($_GET){
    require('../../../../wp-blog-header.php');
    require('db.php');
    require('functions.php');

    global $wpdb;

    $user = get_current_user_id();
    $uri = $_GET['uri'];
    $limit = isset($_GET['limit']) ? " limit ".$_GET['limit'] : '';

    $annotations = $wpdb->get_results("SELECT * FROM ".$db_table." WHERE user=".$user." AND uri='".$uri."'".$limit);

    $annotations = map_annotations($annotations, true);

    $annotations_count = $wpdb->get_var("SELECT COUNT(*) FROM ". $db_table ." WHERE user=". $user . " AND uri='". $uri ."'");

    $result = array(
        'total' => $annotations_count,
        'rows' => $annotations
    );

    header_ok_json();

    echo json_encode($result, true);

    exit();

} else {
    header_notFound();
}