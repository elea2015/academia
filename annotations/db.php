<?php
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

$db_table = 'annotations';

$sql = "CREATE TABLE IF NOT EXISTS `$db_table` (
          `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `user` INT NOT NULL,
          `quote` VARCHAR(255),
          `text` VARCHAR(255),
          `uri` VARCHAR(255),
          `range_start` VARCHAR(255),
          `range_end` VARCHAR(255),
          `start_offset` VARCHAR(255),
          `end_offset` VARCHAR(255),
          `updated` VARCHAR(32),
          `created` VARCHAR(32)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

dbDelta($sql);