<?php
require('functions.php');

$vars = get_post_data();

if(!empty($vars)){
    require('../../../../wp-blog-header.php');
    require('db.php');

    global $wpdb;

    $quote = $vars->quote;
    $ranges = $vars->ranges[0];
    $text = $vars->text;
    $uri = $vars->uri;
    $time = gmdate(DATE_ATOM);

    if($wpdb->insert(
        $db_table,
        array (
            'user' => get_current_user_id(),
            'quote' => $quote,
            'text' => $text,
            'uri' => $uri,
            'range_start' => $ranges->start,
            'range_end' => $ranges->end,
            'start_offset' => $ranges->startOffset,
            'end_offset' => $ranges->endOffset,
            'updated' => $time,
            'created' => $time
        ),
        array (
            '%d',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s'
        )
    )) {
        $annotation_id = $wpdb->insert_id;
        header_seeOther($annotation_id);
    } else
        header_notFound();

} else {
    header_notFound();
}