<?php
require('functions.php');

$putData = get_put_data();

if(!empty($putData)){
    require('../../../../wp-blog-header.php');
    require('db.php');

    global $wpdb;

    $id = $putData['id'];

    if ($wpdb->update(
        $db_table,
        array (
            'text' => $putData['text'],
            'updated' => $putData['updated'],
        ),
        array(
            'id' => $id,
            'user' => $putData['user'],
            'uri' => $putData['uri']
        ),
        array (
            '%s',
            '%s'
        ),
        array(
            '%d',
            '%d',
            '%s'
        )
    ))
        header_seeOther($id);
    else
        header_notFound();

} else {
    header_notFound();
}

