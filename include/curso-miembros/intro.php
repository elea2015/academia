<?php set_lessons_accordion(); global $lessonsAccordion; global $completedLessons; global $lessonsCount;?>

<!-- <img class="onlyMobile" src="<?php //echo get_template_directory_uri(); ?>/img/hero/xavierbgmiembrosmovil.jpg" alt=""> -->
<div class="curso-intro curso-miembros curso-miembrosBg">
    <div class="container xavier-lesson">
        <div class="row ab-container">
            <?php $course_id = get_the_ID();?>
            <div class="title-container">
                <!-- <span class="playButton"><?php //echo do_shortcode('[uo_course_resume course_id="' .$course_id. '"]')?></span>
                <h1 class="subtitle"><?php //echo do_shortcode('[uo_course_resume course_id="' .$course_id. '"]')?></h1> -->
                <h2 class="title mt-2"><?php the_title();?></h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row curso-info justify-content-center">
            <div class="col-sm-4 text-center">
                <?php if (! is_user_logged_in()): ?>
                <div class="curso-icon">
                    <p class="lessons-completed"><?php echo $lessonsCount;?></p>
                </div>
                <h3 class="info-title">Lecciones</h3>
                <?php else : ?>
                <div class="curso-icon">
                    <p class="lessons-completed"><?php echo sprintf("%02d/%02d", $completedLessons, $lessonsCount);?></p>
                </div>
                <h3 class="info-title">Lecciones completadas</h3>
                <?php endif; ?>
                <p>
                    Completa las lecciones a tu propio ritmo
                </p>
            </div>
            <div class="col-sm-4 text-center">
                <div class="curso-icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/monitor.svg" alt="">
                </div>
                <h3 class="info-title">Lecciones en video</h3>
                <p>Lecciones diseñadas con un estilo simple y único de aprendizaje</p>
            </div>
            <div class="col-sm-4 text-center d-none">
                <div class="curso-icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/users-group.svg" alt="">
                </div>
                <h3 class="info-title">Foro</h3>
                <p class="info-hint">
                    Practica e interactúa con otros miembros de la comunidad
                </p>
                <?php if (is_user_logged_in()): ?>
                <p><a class="btn btn-white mx-auto" href="#" target="_blank">Foro</a></p>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>