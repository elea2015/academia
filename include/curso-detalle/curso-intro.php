<div class="container homeVideoContainer">
    <div class="row">
        <div class="col-sm-12">
            <a id="closeVideo" onclick="stopVideo()" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
            <div class="homeVideo">
                <div id="player"></div>
            </div> 
             <script src="http://www.youtube.com/player_api"></script>

            <script type="text/javascript">

            $(document).ready( function() {
                console.log( "ready!" );
                loadPlayer();
                });


            function toggleVideo(){
                $( ".homeVideoContainer" ).slideToggle( "fast", function() {});
            }

            function getArtistId() {
             return 'l-gQLqv9f4o';
            }

            function loadPlayer() { 
            if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {

                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                window.onYouTubePlayerAPIReady = function() {
                onYouTubePlayer();
                };

            } else {
                    onYouTubePlayer();
                }
            }

            var player;

            function onYouTubePlayer() {
            player = new YT.Player('player', {
                height: '490',
                width: '880',
                videoId: getArtistId(),
                playerVars: { controls:1, showinfo: 0, rel: 0, showsearch: 0, iv_load_policy: 3 },
                events: {
                'onStateChange': onPlayerStateChange,
                'onError': catchError
                }
            });
            }

            var done = false;
            function onPlayerStateChange(event) {
                if (event.data == YT.PlayerState.PLAYING && !done) {
                // setTimeout(stopVideo, 6000);
                done = true;
                } else if (event.data == YT.PlayerState.ENDED) {
                    toggleVideo();
                }
            }

            function onPlayerReady(event) {

                //if(typeof(SONG.getArtistId()) == undefined)
                //{
                //  console.log("undefineeeed"); 
                //} 
                //event.target.playVideo();   
            }
            function catchError(event)
            {
                if(event.data == 100) console.log("De video bestaat niet meer");
            }

            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                //nothing
            }else{
                function stopVideo() {
                    player.stopVideo();
                }
            }

             function startVideo() {
                player.playVideo();
            }
            </script>
        </div>
    </div>
</div>
<div class="curso-intro curso-intro-bg" style="background: url('<?php the_field("top_bg"); ?>')">
    <div class="container xavier-lesson">
        <div class="row ab-container">
            <div class="col-sm-12 half-right align-bot">
                <h1 class="title big">Contablilidad para Pymes</h1>
                <h1 class="subtitle"><?php the_field('titulo_del_curso') ?></h1>
                <a id="openVideo" onclick="startVideo()" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/play-button.png" alt=""></a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row curso-info">
            <div class="col-sm-4 text-center">
                <div class="curso-icon">
                    <img src="<?php the_field('video_icon');?>" alt="">
                </div>
                <h3 class="info-title">Lecciones en v&iacute;deo</h3>
                <p>
                   <?php the_field('video_lecciones'); ?>
                </p>
            </div>
            <div class="col-sm-4 text-center">
                <div class="curso-icon">
                    <img src="<?php the_field('contenido_icon');?>" alt="">
                </div>
                <h3 class="info-title">Contenido</h3>
                <p>
                    <?php the_field('contenido'); ?>
                </p>
            </div>
            <div class="col-sm-4 text-center fixedElement">
                <h3 class="class-price"><span>$<?php the_field('precio'); ?></span> <?php the_field('anual_mensual'); ?></h3>
                <p class="class-subinfo"><?php the_field('modo_facturacion');?></p>
                <a href="<?php the_field('link_formulario_de_compra'); ?>" class="btn btn-orange text-uppercase">Toma la clase</a>
               <!--  <a href="#" class="btn btn-gray">Regala la prosperidad</a> -->
                <p class="class-subinfo"><?php the_field('acceso'); ?></p>
            </div>
        </div>
    </div>
</div>

<!-- finanzas al alcanze de todos -->


<!--  end offinanzas al alcanze de todos -->
