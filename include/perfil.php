<div class="perfil">
    <div class="avatar">
        <?php if ( is_null( $user_id ) ) {
					$current_user = wp_get_current_user();
					$user_id = $current_user->ID;
				}
				echo get_avatar( $user_id, 150 );
				?>
    </div>
    <h1 class="title name"><?php echo do_shortcode('[memb_contact fields="FirstName,LastName" separator=" "]'); ?></h1>
    <a href="<?php echo get_site_url(); ?>/editar-perfil">Editar perfil</a>
    <a href="<?php echo get_site_url(); ?>/mi-cuenta">Mi Cuenta</a>
</div>
