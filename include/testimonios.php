<div class="testimonios">
    <div class="container">
    	<div class="row">
    		<div class="col-md-8 col-md-offset-2">
    			<?php echo do_shortcode('[testimonial_rotator id="798"]');?>
    		</div>
    	</div>
    </div>
</div>