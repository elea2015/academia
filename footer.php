		<?php if(!is_page_template(array('page-login.php', 'page-registro.php', 'page-password.php'))) : ;?>
			<!-- footer -->
			<footer class="footer pt-5 pb-3" role="contentinfo">
				<div class="logo-container">
					<!-- logo -->
					<div class="logo">
						<a href="<?php echo home_url(); ?>">
							<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							<img src="<?php echo get_template_directory_uri(); ?>/img/logobpd.png" alt="Logo" class="logo-img">
						</a>
					</div>
					<!-- /logo -->
				</div>
				<div class="main-foot">
					<div class="container">
					    <div class="row justify-content-center d-none">
							<div class="col-sm-4">
								<p>Con&eacute;ctate, interact&uacute;a y &uacute;nete al movimiento.</p>
								<ul class="list-inline text-center">
									<li class="list-inline-item"><a href="#" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li class="list-inline-item"><a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li class="list-inline-item"><a href="#" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								</ul>
								<br>
							</div>
					    </div>
						<div class="row secondFooter">
							<div class="col-12 text-center">
								<a href="/terminos-y-condiciones/">Términos de uso</a> | <a href="/politica-de-privacidad/">Políticas de privacidad</a> | <a href="/soporte/">Soporte</a>
							</div>
						</div>
					</div>
				</div>
				<div class="container cclogo">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-1 col-sm-2 text-center mb-3"><img src="<?php echo get_template_directory_uri()?>/img/vs.svg" alt=""></div>
						<div class="col-lg-1 col-sm-2 text-center mb-3"><img src="<?php echo get_template_directory_uri()?>/img/mc_idcheck_vrt_rev.svg" alt=""></div>
					</div>
				</div>
			</footer>
			<!-- /footer -->
		<?php endif;?>
		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>
		<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/b/javascripts/bootstrap.js"></script>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>
		<button class="scrollToTop"  id="myBtn" title="Go to top"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
	</body>
</html>
