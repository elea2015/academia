<?php get_header(); ?>

	<main role="main" class="regular-page clearfix">
		<!-- section -->
		<section class="wooMain">
			<?php woocommerce_content(); ?>
		</section>
	
	</main>

<?php get_footer(); ?>
