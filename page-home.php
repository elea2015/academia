<?php /* Template Name: Inicio */;?>
<?php get_header(); ?>

<main role="main">
    <div class="container homeVideoContainer">
        <div class="row">
            <div class="col-sm-12">
                <a id="closeVideo" onclick="stopVideo()" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                <div class="homeVideo">
                    <div id="player"></div>
                </div> 
                <script src="http://www.youtube.com/player_api"></script>

                <script type="text/javascript">
                $(document).ready( function() {
                    console.log( "Loaded!" );
                    loadPlayer();
                });
                function toggleVideo(){
                    $( ".homeVideoContainer" ).slideToggle( "fast", function() {});
                }
                function getArtistId() {
                return 'SIZYd-Mxskw';
                }
                function loadPlayer() { 
                if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
                    var tag = document.createElement('script');
                    tag.src = "https://www.youtube.com/iframe_api";
                    var firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                    window.onYouTubePlayerAPIReady = function() {
                    onYouTubePlayer();
                    };
                } else {
                        onYouTubePlayer();
                    }
                }
                var player;
                function onYouTubePlayer() {
                    player = new YT.Player('player', {
                        height: '490',
                        width: '880',
                        videoId: getArtistId(),
                        playerVars: { controls:0, showinfo: 0, rel: 0, showsearch: 0, iv_load_policy: 3, modestbranding: 1 },
                        events: {
                        'onStateChange': onPlayerStateChange,
                        'onError': catchError
                        }
                    });
                }
                var done = false;
                function onPlayerStateChange(event) {
                    if (event.data == YT.PlayerState.PLAYING && !done) {
                    // setTimeout(stopVideo, 6000);
                    done = true;
                    } else if (event.data == YT.PlayerState.ENDED) {
                        toggleVideo();
                    }
                }
                function onPlayerReady(event) {

                    //if(typeof(SONG.getArtistId()) == undefined)
                    //{
                    //  console.log("undefineeeed"); 
                    //} 
                    //event.target.playVideo();   
                }
                function catchError(event)
                {
                    if(event.data == 100) console.log("De video bestaat niet meer");
                }

                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    //nothing
                }else{
                    function stopVideo() {
                        player.stopVideo();
                    }
                }

                function startVideo() {
                    player.playVideo();
                }
                </script>
            </div>
        </div>
    </div>
    <div class="curso-intro curso-intro-bg" style="background: url('<?php the_field("top_bg"); ?>')">
    <?php if(get_field('video_bg')):?>
        <video id="video_bg" autoplay muted loop src="<?php the_field("video_bg"); ?>"></video>
    <?php endif;?>
        <div class="container bpd-lesson">
            <div class="row ab-container">
                <div class="col-sm-12 half-right align-bot">
                    <h1 class="title big">Programa Gestión Integral Pyme</h1>
                    <p class="description">Bienvenidos al Programa Gestión Integral Pyme, programa diseñado para introducirles en el apasionante mundo de la gestión en el ámbito empresarial, proporcionándoles las bases y fundamentos para tener una visión general de la gestión de empresas. Contenido elaborado por el claustro de profesores y colaboradores académicos de Barna Management School.</p>
                    <a id="openVideo" onclick="startVideo()" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/play-button.png" alt=""></a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row curso-info align-items-center">
                <div class="col-md-4 text-center">
                    <div class="curso-icon">
                        <img src="<?php the_field('video_icon');?>" alt="">
                    </div>
                    <h3 class="info-title">Lecciones en video</h3>
                    <p>
                    <?php //the_field('video_lecciones'); ?>
                    </p>
                </div>
                <div class="col-md-4 text-center">
                    <div class="curso-icon">
                        <img src="<?php the_field('contenido_icon');?>" alt="">
                    </div>
                    <h3 class="info-title">Material didáctico</h3>
                    <p>
                        <?php //the_field('contenido'); ?>
                    </p>
                </div>
                <div class="col-md-4 text-center fixedElement">
                    <h3 class="class-price"><span>Regístrate</h3>
                    <p class="class-subinfo d-none"><?php the_field('modo_facturacion');?></p>
                    <a href="/checkout/?add-to-cart=706" class="btn btn-blue text-uppercase mx-auto">Comprar</a>
                    <p class="class-subinfo d-none"><?php the_field('acceso'); ?></p>
                </div>
            </div>
        </div>
    </div>

    <div id="courses" class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h2 class="my-5 blue text-center">Contenido del programa</h2> 
                <?php echo do_shortcode('[ld_course_list orderby="date" order="DESC" col="3"]')?>
            </div>
        </div>
    </div>
    
</main>

<?php get_footer(); ?>
