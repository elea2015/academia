<?php
/**
 * Permissions are NOT handled here!
 */
Class MediaUpload {
    public function __construct() {
    }

    public function saveUpload( $field_name=null, $user_id=null ) {
        if ( is_null($field_name) ) {
            die('Need file');
        }

        global $wpdb;

        // Move the file to the uploads directory, returns an array of information from $_FILES
        $uploaded_file = $this->handleUpload( $_FILES[ $field_name ] );

        if( ! isset( $uploaded_file['file'] ) )
            return false;

        // If we were to have an unique user account for uploading
        if ( is_null( $user_id ) ) {
            $current_user = wp_get_current_user();
            $user_id = $current_user->ID;
        }

        // Build the Global Unique Identifier
        $guid = $this->buildGuid( $uploaded_file['file'] );

        // Build our array of data to be inserted as a post
        $attachment = array(
            'post_mime_type' => $_FILES[ $field_name ]['type'],
            'guid' => $guid,
            'post_title' => 'Avatar - ' . $this->mediaTitle( $uploaded_file['file'] ),
            'post_content' => '',
            'post_author' => $user_id,
            'post_status' => 'inherit',
            'post_date' => date( 'Y-m-d H:i:s' ),
            'post_date_gmt' => date( 'Y-m-d H:i:s' )
        );

        // Add the file to the media library and generate thumbnail.
        $attach_id = wp_insert_attachment( $attachment, $uploaded_file['file'] );

        require_once(ABSPATH. "wp-admin" . '/includes/image.php');
        $meta = wp_generate_attachment_metadata( $attach_id, $uploaded_file['file'] );

        $image = new ImageMeta;
        $meta['image_meta']['keywords'] = $image->iptcParser( 'keywords', $uploaded_file['file'] );
        $meta['image_meta']['city'] = $image->iptcParser( 'city', $uploaded_file['file'] );
        $meta['image_meta']['region'] = $image->iptcParser( 'region', $uploaded_file['file'] );
        $meta['image_meta']['country'] = $image->iptcParser( 'country', $uploaded_file['file'] );

        wp_update_attachment_metadata( $attach_id, $meta );
        $this->avatars_create_avatar(get_current_blog_id(), $uploaded_file['file']);

        // Set the feedback flag to false, since the upload was successful
        $upload_feedback = false;

        return $attach_id;
    }

    public function handleUpload( $file=array() ) {
        require_once(ABSPATH. "wp-admin" . '/includes/file.php');
        return wp_handle_upload( $file, array( 'test_form' => false), date('Y/m'));
    }

    public function buildGuid( $file=null ) {
        $wp_upload_dir = wp_upload_dir();
        return $wp_upload_dir['baseurl'] . '/' . _wp_relative_upload_path( $file );
    }

    public function mediaTitle( $file ) {
        $filename = pathinfo($file);
        return $filename['filename'];
    }

    private function avatars_create_avatar($blog_id, $file) {
        // create avatar directory
        $avatar_path = './wp-content/avatars/blog/'.substr(md5($blog_id), 0, 3);
        if (!is_dir($avatar_path)){
            mkdir($avatar_path, 0777);
        }
        // create avatar in sizes
        $this->avatars_create_avatar_size($blog_id, $avatar_path, $file, 150);
        $this->avatars_create_avatar_size($blog_id, $avatar_path, $file, 128);
        $this->avatars_create_avatar_size($blog_id, $avatar_path, $file, 96);
        $this->avatars_create_avatar_size($blog_id, $avatar_path, $file, 48);
        $this->avatars_create_avatar_size($blog_id, $avatar_path, $file, 32);
        $this->avatars_create_avatar_size($blog_id, $avatar_path, $file, 16);
    }
    private function avatars_create_avatar_size($blog_id, $avatar_path, $newImg, $size) {
        $width = imagesx($newImg);
        $height = imagesy($newImg);
        $im_dest = imagecreatetruecolor ($size, $size);
        imagecopyresampled($im_dest, $newImg, 0, 0, 0, 0, $size, $size, $width, $height);
        if ($image_type == 'png'){
            imagesavealpha($im_dest, true);
        }
        $image = imagepng($im_dest, $avatar_path.'/blog-' . $blog_id . "-$size.png");
    }
}