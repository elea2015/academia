<?php /* Template Name: Registro */;?>
<?php get_header(); ?>
<main role="main" id="membLogin">
	<img class="logo" src="<?php echo get_template_directory_uri(); ?>/img/logobpd.png" alt="Logo" class="logo-img">
    <section class="text-center">
    	<h3>Registro</h3>
        <br>
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; ?>
		<?php endif; ?>
        <a class="forgotPassword" href="/login/" title="Lost Password">Iniciar sesión</a>
    </section>
</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>