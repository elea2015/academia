<?php /* Template Name: Perfil */;?>
<?php get_header(); ?>

<div class="container">
    <div class="row justify-content-between">
        <div class="col-md-3">
            <div class="perfil">
                <div class="avatar">
                    <?php 
                        if ( is_null( $user_id ) ) {
                            $current_user = wp_get_current_user();
                            $user_id = $current_user->ID;
                        }
                        echo get_avatar( $user_id, 150 );
                    ?>
                </div>
                <h1 class="title name"><?php echo do_shortcode('[memb_contact fields="FirstName,LastName" separator=" "]'); ?></h1>
                <a class="btn btn-blue" href="<?php echo get_site_url(); ?>/editar-perfil">Editar perfil</a>
            </div>
        </div>
        <div class="col-md-8">
            <div class="mis-clases py-5">
                <?php echo do_shortcode('[memb_set_prohibited_action action=show]');?>
                <?php echo do_shortcode('[ld_course_list orderby="date" order="DESC" col="2"]')?>
                <?php echo do_shortcode('[memb_sync_contact]'); ?>
            </div>
        </div>
    </div>
</div>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>