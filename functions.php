<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

function frontend_login_script( $user, $user_id ) {

    if( $user ) {
        $login_message = apply_filters( 'appp_login_success', sprintf( __('Welcome back %s!', 'apppresser'), $user->display_name), $user->ID );
        $username = $user->user_login;
        $avatar_url = get_avatar_url( $user->ID );
        $login_redirect = appp_get_login_redirect();

        ?>
        <script type="text/javascript">

        var login_data = {
        message: '<?php echo $login_message; ?>', // welcome message
        username: '<?php echo $username; ?>', // name used in greeting
        avatar: '<?php echo $avatar_url; ?>', // URL to the avatar
        login_redirect: '<?php echo $login_redirect; ?>' // optional login redirect
        }

        window.apppresser.sendLoginMsg( true, login_data );

        </script><?php 
    }

}


// function appp_get_login_redirect() {

//     if( has_filter( 'appp_login_redirect' ) ) {
//         $redirect_to = apply_filters( 'appp_login_redirect', '' );
//         $post_id = url_to_postid( $redirect_to );

//         $redirect = array(
//             'url' => $redirect_to,
//             'title' => ($post_id) ? get_the_title( $post_id ) : '',
//         );  
        
//     } else {
//         $redirect = '';
//     }

//     return $redirect;
// }

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 700, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}
function footer_menu()
{
    wp_nav_menu(
        array(
            'theme_location'  => 'extra-menu',
            'menu'            => 'Footer Menu',
            'container'       => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul>%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        )
    );
}
// Load HTML5 Blank scripts (header.php)
function header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

	    wp_deregister_script('jquery');
        wp_register_script('jquery', get_template_directory_uri() . '/js/lib/jquery-2.2.4.min.js', false, '2.2.4');
        wp_enqueue_script('jquery');

        wp_register_script('jquery-ui', get_template_directory_uri() . '/js/lib/jquery-ui-1.12.1.accordion/jquery-ui.min.js', array('jquery'), '1.12.1');
        wp_enqueue_script('jquery-ui');

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        // wp_register_script('annotator', get_template_directory_uri() . '/js/lib/annotator-1.2.10/annotator-full.min.js', array('jquery'), '1.2.10');
        // wp_enqueue_script('annotator'); // Enqueue it!

        // wp_register_script('annotator-touch', get_template_directory_uri() . '/js/lib/annotator-1.2.10/annotator.touch.min.js', array('annotator'), '1.1.1');
        // wp_enqueue_script('annotator-touch'); // Enqueue it!

        // wp_register_script('annotator-highlighter', get_template_directory_uri() . '/js/lib/annotator-1.2.10/highlighter.js', array('annotator-touch'), '1.1.1');
        // wp_enqueue_script('annotator-highlighter'); // Enqueue it!

        wp_register_script('scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_localize_script('scripts', 'wp_vars', array('template_dir' => parse_url(get_template_directory_uri())['path']));
        wp_enqueue_script('scripts'); // Enqueue it!
    }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('fontawesome', get_template_directory_uri() . '/assets/font-awesome-4.7.0/css/font-awesome.min.css', array(), '4.7.0', 'all');
    wp_enqueue_style('fontawesome'); // Enqueue it!

    wp_register_style('jquery-ui', get_template_directory_uri() . '/js/lib/jquery-ui-1.12.1.accordion/jquery-ui.min.css', array(), '1.12.1', 'all');
    wp_enqueue_style('jquery-ui'); // Enqueue it!

    wp_register_style('annotatortouch', get_template_directory_uri() . '/js/lib/annotator-1.2.10/annotator.touch.css', array(), '1.1.1', 'all');
    wp_enqueue_style('annotatortouch'); // Enqueue it!

    wp_register_style('style', get_template_directory_uri() . '/style.css', array(), '1.1', 'all');
    wp_enqueue_style('style'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}
function set_lessons_accordion() {

    global $lessonsAccordion;
    global $completedLessons;
    global $lessonsCount;

    if (!empty($lessonsAccordion)) return;

    $course_id = get_the_id();
    $lessons = learndash_get_course_lessons_list( $course_id );
    $completedLessons = 0;
    $lessonsCount = count($lessons);
    $lessonsAccordion = [];
    $count = 0;

    if (!empty($lessons)) {
        /** @var array $lessons */
        foreach($lessons as $lesson) {
            $lessonsAccordion[$count]['title'] = $lesson['post']->post_title;
            $lessonsAccordion[$count]['description'] = $lesson['post']->post_content;
            $lessonsAccordion[$count]['topics'] = learndash_get_topic_list($lesson['post']->ID);
            $lessonsAccordion[$count]['status'] = 'nocompleted';
            $lessonsAccordion[$count]['link'] = $link = get_permalink($lesson['post']->ID);

            if ($lesson['status'] == 'completed') {
                $lessonsAccordion[$count]['status'] = 'completed';
                $completedLessons++;
            }

            $count++;
        }
    }
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Foro Reciente', 'html5blank'),
        'description' => __('Foros recientes', 'html5blank'),
        'id' => 'foro-reciente',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Foro Popular', 'html5blank'),
        'description' => __('Foro popular...', 'html5blank'),
        'id' => 'foro-popular',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	</div>
    <div class="comment-content">
        <div class="comment-author-name">
            <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
        </div>

        <?php if ($comment->comment_approved == '0') : ?>
            <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
            <br />
        <?php endif; ?>

        <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                <?php
                printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
            ?>
        </div>

        <?php comment_text() ?>

        <div class="reply">
            <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
        </div>
    </div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }
function newrs_add_custom_variables($m, $data, $options) {
    $m->addHelper('testimonio_image', function() use ($data) {
        if(get_field('avatar', $data->ID)) {
            return get_field('avatar', $data->ID);
        }
        return '';
    } );
    $m->addHelper('testimonio_text', function() use ($data) {
        if(get_field('testimonio', $data->ID)) {
            return get_field('testimonio', $data->ID);
        }
        return '';
    } );
    $m->addHelper('testimonio_name', function() use ($data) {
        if(get_field('nombre', $data->ID)) {
            return get_field('nombre', $data->ID);
        }
        return '';
    } );
    $m->addHelper('testimonio_subtitle', function() use ($data) {
        if(get_field('subtitulo', $data->ID)) {
            return get_field('subtitulo', $data->ID);
        }
        return '';
    } );
}

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('new_rs_slides_renderer_helper', 'newrs_add_custom_variables', 10, 4);

add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type()
{
    register_taxonomy_for_object_type('category', 'testimonios'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'testimonios');
    register_post_type('testimonios', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Testimonios', 'testimonios'), // Rename these to suit
            'singular_name' => __('Testimonio', 'testimonios'),
            'add_new' => __('Add New', 'testimonios'),
            'add_new_item' => __('Add New Testimonio', 'testimonios'),
            'edit' => __('Edit', 'testimonios'),
            'edit_item' => __('Edit Testimonio', 'testimonios'),
            'new_item' => __('New Testimonio', 'testimonios'),
            'view' => __('View Testimonio', 'testimonios'),
            'view_item' => __('View Testimonio', 'testimonios'),
            'search_items' => __('Search Testimonio', 'testimonios'),
            'not_found' => __('No Testimonio found', 'testimonios'),
            'not_found_in_trash' => __('No Testimonio found in Trash', 'testimonios')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_video()
{
    register_taxonomy_for_object_type('category', 'videos'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'videos');
    register_post_type('videos', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Videos', 'videos'), // Rename these to suit
            'singular_name' => __('Video', 'videos'),
            'add_new' => __('Add New', 'videos'),
            'add_new_item' => __('Add New Video', 'videos'),
            'edit' => __('Edit', 'videos'),
            'edit_item' => __('Edit Video', 'videos'),
            'new_item' => __('New Video', 'videos'),
            'view' => __('View Video', 'videos'),
            'view_item' => __('View Video', 'videos'),
            'search_items' => __('Search Video', 'videos'),
            'not_found' => __('No video found', 'videos'),
            'not_found_in_trash' => __('No video found in Trash', 'videos')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => false,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author',
            'comments',
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

//add_action('init', 'create_post_type_video');

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_filter ('add_to_cart_redirect', 'redirect_to_checkout');

function redirect_to_checkout() {
    global $woocommerce;
    $checkout_url = $woocommerce->cart->get_checkout_url();
    return $checkout_url;
}

add_filter( 'woocommerce_new_customer_data', function( $data ) {
  $data['user_login'] = $data['user_email'];

  return $data;
} );


//remove Order Notes from checkout field in Woocommerce
add_filter( 'woocommerce_checkout_fields' , 'alter_woocommerce_checkout_fields' );
function alter_woocommerce_checkout_fields( $fields ) {
    //  unset($fields['billing']['razon-social']);
    //  unset($fields['billing']['rnc']);
     return $fields;
}

//Use email as username woocomerce checkout
add_filter( 'woocommerce_new_customer_data', function( $data ) {
  $data['user_login'] = $data['user_email'];

  return $data;
} );
//bootstrap nav bar
/* Theme setup */
add_action( 'after_setup_theme', 'wpt_setup' );
    if ( ! function_exists( 'wpt_setup' ) ):
        function wpt_setup() {  
            register_nav_menu( 'foro', __( 'Foro menu', 'wptuts' ) );
        } endif;


require_once('wp_bootstrap_navwalker.php');

//redirect
// add_action( 'template_redirect', 'redirect_to_specific_page' );

// function redirect_to_specific_page() {

// if ( ! is_user_logged_in() ) {

// wp_redirect( 'http://xavierserbia.com/clase-magistral/', 307 ); 
//   exit;
//   }
// }

function bbp_enable_visual_editor( $args = array() ) {
    $args['tinymce'] = true;
    return $args;
}
add_filter( 'bbp_after_get_the_content_parse_args', 'bbp_enable_visual_editor' );


// add_filter ('the_content', 'insertSubscribeNewsLetter');
// function insertSubscribeNewsLetter($content) {
//    if(is_singular( 'post' )) {
//       $content.= '[acf field="video_field"]';
//    }
//    return $content;
// }
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//woocomerce auto registrtion
function wc_register_guests( $order_id ) {
  // get all the order data
  $order = new WC_Order($order_id);
  
  //get the user email from the order
  $order_email = $order->billing_email;
    
  // check if there are any users with the billing email as user or email
  $email = email_exists( $order_email );  
  $user = username_exists( $order_email );
  
  // if the UID is null, then it's a guest checkout
  if( $user == false && $email == false ){
    
    // random password with 12 chars
    $random_password = wp_generate_password();
    
    // create new user with email as username & newly created pw
    $user_id = wp_create_user( $order_email, $random_password, $order_email );
    
    //WC guest customer identification
    update_user_meta( $user_id, 'guest', 'yes' );
 
    //user's billing data
    update_user_meta( $user_id, 'billing_address_1', $order->billing_address_1 );
    update_user_meta( $user_id, 'billing_address_2', $order->billing_address_2 );
    update_user_meta( $user_id, 'billing_city', $order->billing_city );
    update_user_meta( $user_id, 'billing_company', $order->billing_company );
    update_user_meta( $user_id, 'billing_country', $order->billing_country );
    update_user_meta( $user_id, 'billing_email', $order->billing_email );
    update_user_meta( $user_id, 'billing_first_name', $order->billing_first_name );
    update_user_meta( $user_id, 'billing_last_name', $order->billing_last_name );
    update_user_meta( $user_id, 'billing_phone', $order->billing_phone );
    update_user_meta( $user_id, 'billing_postcode', $order->billing_postcode );
    update_user_meta( $user_id, 'billing_state', $order->billing_state );
 
    // user's shipping data
    update_user_meta( $user_id, 'shipping_address_1', $order->shipping_address_1 );
    update_user_meta( $user_id, 'shipping_address_2', $order->shipping_address_2 );
    update_user_meta( $user_id, 'shipping_city', $order->shipping_city );
    update_user_meta( $user_id, 'shipping_company', $order->shipping_company );
    update_user_meta( $user_id, 'shipping_country', $order->shipping_country );
    update_user_meta( $user_id, 'shipping_first_name', $order->shipping_first_name );
    update_user_meta( $user_id, 'shipping_last_name', $order->shipping_last_name );
    update_user_meta( $user_id, 'shipping_method', $order->shipping_method );
    update_user_meta( $user_id, 'shipping_postcode', $order->shipping_postcode );
    update_user_meta( $user_id, 'shipping_state', $order->shipping_state );
    
    // link past orders to this newly created customer
    wc_update_new_customer_past_orders( $user_id );
  }
  
}
 
//add this newly created function to the thank you page
add_action( 'woocommerce_thankyou', 'wc_register_guests', 10, 1 );

/*Change thank you message*/

add_filter( 'woocommerce_thankyou_order_received_text', 'avia_thank_you' );
function avia_thank_you() {
 $added_text = '<h2 class="blue">Gracias por tu pedido</h2><p class="custom_order_thanks">Has completado de forma satisfactoria el pago de la inscripción del <strong>Programa Gestión Integral Pyme</strong>, te invitamos a navegar en el módulo Análisis de Situaciones en los Negocios y conocer todo el contenido multimedia y material soporte que incluye.<br/><br/>En unos momentos estarás recibiendo en tu correo el usuario y la contraseña necesarios para acceder a los contenidos en la plataforma.<br/><br/>Estamos comprometidos con ofrecerte una experiencia memorable de servicio.</p>';
 return $added_text ;
}

/*Woo remove product link from cart*/
add_filter('woocommerce_cart_item_permalink','__return_false');

/*woo remove cart loop*/
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

add_filter('woocommerce_order_item_permalink', function(){
return false;
});

/*change woo add to cart message*/

add_filter(  'gettext',  'change_specific_add_to_cart_notice', 10, 3 );
add_filter(  'ngettext',  'change_specific_add_to_cart_notice', 10, 3 );
function change_specific_add_to_cart_notice( $translated, $text, $domain  ) {
    if( $text === 'You cannot add another "%s" to your cart.' && $domain === 'woocommerce' && ! is_admin() ){
        // Replacement text (where "%s" is the dynamic product name)
        $translated = __( 'Por favor complete su pedido de "%s"', $domain );
    }
    return $translated;
}

add_filter( 'style_loader_src', function( $src, $handle ) {
    // If you know the handle
    // if ( 'your-css-handle' === $handle ) {
    //  return remove_query_arg( 'ver', $src );
    // }
    // return $src;
    return remove_query_arg( 'ver', $src );
}, 10, 2 );

?>