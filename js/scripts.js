(function($, root, undefined) {
    $(function() {
        "use strict";

        // DOM ready, take it away

        var $login = $("#login-page"),
            $document = $(document),
            template_dir = wp_vars.template_dir;

        $login.ready(function(e) {
            $login.height($document.height());
            $(window).resize(function() {
                $login.height($document.height());
            });
        });

        $(".link-height-container").ready(function(e) {
            resize_blocks(".link-height-container", ".link-height");
            $(window).resize(function() {
                resize_blocks(".link-height-container", ".link-height");
            });
        });

        $("a.scroll, li.scroll > a").click(function(e) {
            e.preventDefault();
            $("html, body").animate({
                    scrollTop: $($.attr(this, "href")).offset().top
                },
                500
            );
            return false;
        });
        // if ($(".accordion").length) {
        // 	$(".accordion").accordion({
        // 		collapsible: true,
        // 		heightStyle: "content",
        // 		icons: {
        // 			"header": "fa fa-chevron-right",
        // 			"activeHeader": "fa fa-chevron-down"
        // 		}
        // 	});
        // }

        // if ( $('.annotation-content').length ) {
        // 	var ann = $('.annotation-content').annotator('setupPlugins', null, {
        // 		Permissions: false,
        // 		AnnotateItPermissions: {}
        // 	});
        // 	ann.annotator('addPlugin', 'Store', {
        // 		// The endpoint of the store on your server.
        // 		prefix: template_dir+'/annotations',

        // 		// Attach the uri of the current page to all annotations to allow search.
        // 		annotationData: {
        // 			'uri': window.location.href
        // 		},

        // 		// This will perform a "search" action when the plugin loads. Will
        // 		// request the last 20 annotations for the current url.
        // 		// eg. /store/endpoint/search?limit=20&uri=http://this/document/only
        // 		loadFromSearch: {
        // 			'limit': 20,
        // 			'uri': window.location.href
        // 		},

        // 		urls: {
        // 			// These are the default URLs.
        // 			create:  '/create.php',
        // 			update:  '/update.php',
        // 			destroy: '/destroy.php',
        // 			search:  '/search.php'
        // 		}
        // 	});
        //     ann.annotator('addPlugin', 'Touch', {
        //         force: location.search.indexOf('force') > -1,
        //         useHighlighter: location.search.indexOf('highlighter') > -1
        //     });
        // }
    });

    function resize_blocks(selector, childSelector) {
        var max_height = 0;
        var blocks;

        $(selector).each(function() {
            max_height = 0;
            blocks = $(this).find(childSelector);
            blocks.css("min-height", "inherit");
            if (viewport().width >= 768) {
                blocks.each(function(item) {
                    max_height =
                        $(this).outerHeight() > max_height ?
                        $(this).innerHeight() :
                        max_height;
                });
                blocks.css("min-height", max_height);
            } else {
                blocks.css("min-height", "");
            }
        });
    }

    function viewport() {
        var e = window,
            a = "inner";
        if (!("innerWidth" in window)) {
            a = "client";
            e = document.documentElement || document.body;
        }
        return { width: e[a + "Width"], height: e[a + "Height"] };
    }

    $(document).ready(function() {
        console.log("ready! Yeah");
        //hide video
        if ($("#learndash_complete_prerequisites")[0]) {
            $(".learndashVideo").hide()
        } else {
            // Do something if class does not exist
        }

        //hide video
        if ($("#learndash_complete_prev_lesson")[0]) {
            $(".learndashVideo").hide()
        } else {
            // Do something if class does not exist
        }
        //Check to see if the window is top if not then display button
        $(window).scroll(function() {
            if ($(this).scrollTop() > 200) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });

        //Click event to scroll to top
        $('.scrollToTop').click(function() {
            $('html, body').animate({ scrollTop: 0 }, 800);
            return false;
        });


        $("#openVideo, #closeVideo").click(function(event) {
            event.preventDefault();
            toggleVideo();
        });

        //gravity read only
        $(".gform_wrapper .organicweb-readonly input").attr("readonly", "");

        $(".wpProQuiz_button[value='Reiniciar Cuestionario']").val("Reiniciar cuestionario");

    });
})(jQuery, this);